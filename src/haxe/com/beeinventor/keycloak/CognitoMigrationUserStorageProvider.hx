package com.beeinventor.keycloak;

import sys.thread.Lock;
import sys.thread.Thread;
import org.keycloak.models.UserProvider;
import org.keycloak.storage.user.SynchronizationResult;
import haxe.DynamicAccess;
import java.util.Collections;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserModel;
import org.keycloak.storage.user.UserLookupProvider;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.representations.idm.GroupRepresentation;

class CognitoMigrationUserStorageProvider implements UserStorageProvider implements UserLookupProvider implements CredentialInputValidator {
	
	public static inline final WEBHOOK_BATCH_SIZE = 100;
	
	final session:KeycloakSession;
	final model:ComponentModel;
	final cognito:Cognito;
	final webhooks:Webhooks;

	public function new(session, model, cognito, webhooks) {
		this.session = session;
		this.model = model;
		this.cognito = cognito;
		this.webhooks = webhooks;
	}

	public function close() {
		// do nothing
	}

	public overload function preRemove(realm:RealmModel) {
		// do nothing
	}

	public overload function preRemove(realm:RealmModel, group:GroupModel) {
		// do nothing
	}

	public overload function preRemove(realm:RealmModel, role:RoleModel) {
		// do nothing
	}

	public function isConfiguredFor(realm:RealmModel, user:UserModel, type:String):Bool {
		return supportsCredentialType(type);
	}

	public function isValid(realm:RealmModel, user:UserModel, input:CredentialInput):Bool {
		if (!supportsCredentialType(input.getType()))
			return false;

		final username = user.getUsername();
		final password = input.getChallengeResponse();

		return switch cognito.validate(username, password) {
			case null:
				false;
			case attributes:
				// TODO: old password may not comply with current password policies
				// need to find a way to bypass password policies, or force user to change password
				session.userCredentialManager().updateCredential(realm, user, input);
				user.setEnabled(true);
				user.setEmailVerified(attributes['email_verified'] == 'true');

				setupUser(realm, user, attributes, model);

				user.setFederationLink(null);

				// invoke webhook
				switch webhooks.onCredentialsMigrated {
					case null: // skip
					case webhook:
						final result = webhook.invoke('POST', {
							final headers = [];
							switch webhooks.auth {
								case null | '': // skip
								case v: headers.push({name: 'Authorization', value: v});
							}
							headers;
						}, {
							id: user.getId(),
							attributes: {
								final attr = new DynamicAccess();
								for (key => value in attributes)
									attr['$key'] = value;
								attr;
							}
						});
				}
				true;
		}
	}

	public function supportsCredentialType(type:String):Bool {
		return type == PasswordCredentialModel.TYPE;
	}

	public overload function getUserByEmail(realm:RealmModel, email:String):UserModel {
		return switch cognito.get(email) {
			case null:
				null;
			case remote:
				createUsersIfNotExist(
					session.userLocalStorage(),
					realm,
					model,
					webhooks,
					[{
						username: remote.getUsername(),
						attributes: [for(attr in remote.getUserAttributes()) attr.getName() => attr.getValue()],
					}]
				)[0];
		}
	}

	public overload function getUserByEmail(email:String, realm:RealmModel):UserModel {
		return getUserByEmail(realm, email);
	}

	public overload function getUserById(realm:RealmModel, id:String):UserModel {
		return null;
	}

	public overload function getUserById(id:String, realm:RealmModel):UserModel {
		return getUserById(realm, id);
	}

	public overload function getUserByUsername(realm:RealmModel, username:String):UserModel {
		return null;
	}

	public overload function getUserByUsername(username:String, realm:RealmModel):UserModel {
		return getUserByUsername(realm, username);
	}

	public static function logMessage(message:String) {
		var timeStamp = Date.now().toString();

		trace('${timeStamp} : ${message}');
	}

	public static function createClientGroupIfNotExists(realm:RealmModel, clientName:String, clientShortName: String, clientId:String):GroupModel {
		logMessage('createClientGroupIfNotExists clientName = ${clientName}  clientId = ${clientId}');

		for (group in realm.getGroupsStream().toArray()) {
			if ((group.getName().equals(clientName)) && (group.getFirstAttribute('clientId') == clientId)) {
				return group;
		  	}
		}

		var newGroup = realm.createGroup(clientName);
		newGroup.setSingleAttribute('clientId', clientId);		
		newGroup.setSingleAttribute('clientShortName', clientShortName);
		newGroup.setSingleAttribute('clientName', clientName);

		return newGroup;
	}

	public static function createDealerGroupGroupIfNotExists(realm:RealmModel, clientGroup:GroupModel, dealerGroupName:String, dealerGroupId:String):GroupModel {
		logMessage('createDealerGroupGroupIfNotExists dealerGroupName = ${dealerGroupName}  dealerGroupId = ${dealerGroupId}');

		for (group in realm.getGroupsStream().toArray()) {
			if ((group.getName().equals(dealerGroupName)) && (group.getFirstAttribute('dealerGroupId') == dealerGroupId))  {
				return group;
		  	}
		}

		var newGroup = realm.createGroup(dealerGroupName, clientGroup);
		newGroup.setSingleAttribute('dealerGroupId', dealerGroupId);
		newGroup.setSingleAttribute('dealerGroupName', dealerGroupName);

		return newGroup;
	}

	public static function createDealerGroupIfNotExists(realm:RealmModel, dealerGroup:GroupModel, dealerName:String, dealerId:String):GroupModel {
		logMessage('createDealerGroupIfNotExists ${dealerName} ${dealerId}');
		
		for (group in realm.getGroupsStream().toArray()) {
			if ((group.getName().equals(dealerName)) && (group.getFirstAttribute('dealerId') == dealerId))  {
				return group;
		  	}
		}

		var newGroup = realm.createGroup(dealerName, dealerGroup);
		newGroup.setSingleAttribute('dealerId', dealerId);
		newGroup.setSingleAttribute('dealerName', dealerName);

		return newGroup;
	}
		
	public static function setupUser(realm:RealmModel, user, attributes:Map<String, String>, model:ComponentModel) {
		switch attributes['email'] {
			case null: // skip
			case email: user.setEmail(email);
		}

		var clientId = '';
		var clientName = '';

		var dealerGroupId = '';
		var dealerGroupName = '';
	
		var dealerId = '';
		var dealerName = '';

		var clientShortName = '';

		var addAttribute;

		for (key => value in attributes) {
			if (key.substring(0, 7) == 'custom:') {
				key = key.substring(7);
			}

			addAttribute = false;

			switch key.toLowerCase() {
				case 'dealeruserid': 
					addAttribute = true;
				case 'phone_number': 
					addAttribute = true;

				case 'family_name':
					user.setLastName(value);					
					
				case 'given_name':
					user.setFirstName(value);

				case 'clientid':
					clientId = value;
				case 'clientshortname':
					clientShortName = value;
				case 'clientname':
					clientName = value;
				case 'dealergroupid':
					dealerGroupId = value;
				case 'dealergroupname':
					dealerGroupName = value;
				case 'dealerid':
					dealerId = value;
				case 'dealername':
					dealerName = value;		
			}					
				
			if (addAttribute) {
				user.setAttribute(key, Collections.singletonList(value));
			}
		}


		var client = createClientGroupIfNotExists(realm, clientName, clientShortName, clientId);
		var dealerGroup;

		if (dealerGroupName != '') {
			dealerGroup = createDealerGroupGroupIfNotExists(realm, client, dealerGroupName, dealerGroupId);
		}
		else {
			// not all dealers have a dealer group
			logMessage('createUsersIfNotExist NO Dealer group');

			dealerGroup = client;
		}

		var dealer = createDealerGroupIfNotExists(realm, dealerGroup, dealerName, dealerId);

		logMessage('createUsersIfNotExist Add user to group');
		user.joinGroup(dealer);

		user.setFederationLink(model.getId());
	}

	public static function createUsersIfNotExist(localStorage:UserProvider, realm:RealmModel, model:ComponentModel, webhooks:Webhooks, remoteUsers:Array<{username:String, attributes:Map<String, String>}>, ?result:SynchronizationResult):Array<UserModel> {
		logMessage('createUsersIfNotExist ${remoteUsers.length}');
		final webhookPayloads:Array<Webhooks.UserPayload> = [];
		final ret:Array<UserModel> = [];

		for (remote in remoteUsers) {
			final attributes = remote.attributes;
			if (localStorage.searchForUserByUserAttributeStream(realm, 'cognito_sub', attributes['sub']).count() == 0) {
				if(result != null) result.increaseAdded();
				final username = remote.username;
				final local = localStorage.addUser(realm, username);
				ret.push(local);

				setupUser(realm, local, attributes, model);

				// webhook
				logMessage('add webhook payload ${local.getId()}');
				webhookPayloads.push({
					id: local.getId(),
					attributes: {
						final attr = new DynamicAccess();
						for (key => value in attributes)
							attr['$key'] = value;
						attr;
					}
				});
			}
		}

		// run webhooks in parallel
		final lock = new Lock();
		final batches = Math.ceil(webhookPayloads.length / WEBHOOK_BATCH_SIZE);

		logMessage('invoking $batches webhook(s)');

		for (i in 0...batches) {
			final payloads = webhookPayloads.slice(i * WEBHOOK_BATCH_SIZE, (i + 1) * WEBHOOK_BATCH_SIZE);
			// invoke webhook
			switch webhooks.onAccountCreated {
				case null: // skip
				case webhook:
					final result = webhook.invoke('POST', {
						final headers = [];
						switch webhooks.auth {
							case null | '': // skip
							case v: headers.push({name: 'Authorization', value: v});
						}
						headers;
					}, payloads);
			}
		}
		
		return ret;
	}
}
